/*
 * Copyright © 2020. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

plugins {
    `kotlin-dsl`
}

apply { from("_repositories.gradle.kts") }

dependencies {
    val kotlinVersion: String by project
    val jplBuildUtilsVersion: String by project

    // want to make sure this is on buildscript classpath also for "external" gradle scripts
    implementation(jpl("jaspery-kotlin-build-utils:jaspery-kotlin-mp-build", jplBuildUtilsVersion))

    @Suppress("UnstableApiUsage")
    constraints {
        implementation(kotlin("reflect", kotlinVersion))
        implementation(kotlin("stdlib-jdk8", kotlinVersion))
    }
}

fun DependencyHandlerScope.jpl(simpleModuleName: String, version: String? = null): String =
        "com.jaspery.jaspery-public-libraries.$simpleModuleName" + version?.let { ":$it" }.orEmpty()

