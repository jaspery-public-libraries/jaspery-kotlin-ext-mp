/*
* Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
*/
rootProject.name = "jaspery-kotlin-ext-mp"

include("jaspery-kotlin-lang-mp")

pluginManagement {
    @Suppress("UnstableApiUsage")
    plugins {
        // load versions from gradle.properties
        val kotlinVersion: String by settings
        // declare default plugin versions
        kotlin("multiplatform") version kotlinVersion
    }
}
