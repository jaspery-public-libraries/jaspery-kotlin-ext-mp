/*
 * Copyright © 2020. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

import com.jaspery.gradle.ext.dependencies.threeTenBp
import com.jaspery.gradle.ext.kotlin.jvmMainDependencies

val `310bpVersion`: String by project

kotlin {
    jvmMainDependencies {
        api(threeTenBp(`310bpVersion`))
    }
}

publishing {
    publications.forEach {
        if (it is MavenPublication) {
            if (project.hasProperty("publishToMavenRepo")) {
                val buildBranch: String? by project
                val buildNumber: String by project
                val defaultBranch: String by project

                val versionBranchPart = buildBranch?.takeUnless { it == defaultBranch }?.let { "-$it" } ?: ""
                val versionBuildNumberPart = buildNumber.takeUnless { it.isEmpty() }
                        ?.let { "-${it.padStart(6, '0')}" } ?: ""
                it.version = "${project.version}$versionBuildNumberPart$versionBranchPart"
            }
        }
    }

    repositories {
        if (project.hasProperty("publishToMavenRepo")) {
            val publishMavenRepoUrl: String by project
            val credentialsTokenType: String by project
            val credentialsTokenValue: String by project

            maven(publishMavenRepoUrl) {
                name = "GitLab-${project.name}"
                credentials(HttpHeaderCredentials::class) {
                    name = credentialsTokenType
                    value = credentialsTokenValue
                }
                authentication {
                    create<HttpHeaderAuthentication>("header")
                }
            }
        }
    }
}

