/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.kotlin.date

import kotlin.test.Test
import kotlin.test.assertEquals

class LocalDateTest2 {
    @Test
    fun testParseJs() {
        assertEquals("2018-03-23".date, LocalDate.parse("2018-03-23"))
    }
}
