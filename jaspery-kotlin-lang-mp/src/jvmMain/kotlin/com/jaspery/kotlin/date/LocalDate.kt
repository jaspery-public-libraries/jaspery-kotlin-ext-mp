/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.kotlin.date

private typealias OtbLocalDate = org.threeten.bp.LocalDate

actual class LocalDate(private val platformDate: OtbLocalDate) {
    actual companion object {
        actual fun parse(text: CharSequence): LocalDate {
            return LocalDate(OtbLocalDate.parse(text))
        }

        actual fun of(year: Int, month: Month, dayOfMonth: Int): LocalDate {
            return LocalDate(OtbLocalDate.of(year, month.getValue(), dayOfMonth))
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is LocalDate) return false
        return this.platformDate == other.platformDate
    }

    override fun hashCode(): Int = platformDate.hashCode()

    override fun toString(): String {
        return platformDate.toString()
    }
}
