/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.kotlin.date

import com.jaspery.kotlin.lang.requireValueIn
import kotlin.js.Date

actual class LocalDate(private val platformDate: Date) {
    actual companion object {
        actual fun parse(text: CharSequence): LocalDate {
            val split = text.split("-")
            if (split.size != 3) {
                throw DateTimeParseException()
            }
            try {
                val y = split[0].toInt()
                val m = requireValueIn(split[1].toInt(), 1..12)
                val month = Month.of(m)
                val monthLength = month.length(Year.isLeap(y))
                val d = requireValueIn(split[2].toInt(), 1..monthLength)
                return of(y, month, d)
            } catch (e: IllegalArgumentException) {
                throw DateTimeParseException()
            }
        }

        actual fun of(year: Int, month: Month, dayOfMonth: Int): LocalDate {
            return LocalDate(Date(year, month.getOrdinal(), dayOfMonth))
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is LocalDate) return false
        return this.platformDate.getTime() == other.platformDate.getTime()
    }

    override fun hashCode(): Int {
        val y = platformDate.getFullYear()
        val m = platformDate.getMonth()
        val d = platformDate.getDate()
        return arrayOf(y, m, d).fold(1) { r, e -> 31 * r + e }
    }

    override fun toString(): String {
        val y = platformDate.getFullYear()
        val m = platformDate.getMonth() + 1
        val d = platformDate.getDate()
        val mStr = "$m".padStart(2, '0')
        val dStr = "$d".padStart(2, '0')
        return "$y-$mStr-$dStr"
    }
}
