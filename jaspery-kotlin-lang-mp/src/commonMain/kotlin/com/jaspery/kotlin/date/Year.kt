/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.kotlin.date

import com.jaspery.kotlin.lang.requireValueIn

@Suppress("EXPERIMENTAL_FEATURE_WARNING")
inline class Year
@Deprecated(
        "Due to limitation of inline classes constructor does not check parameter. " +
                "Prefer using Year.of(isoYear)",
        ReplaceWith("Year.of(value)"))
constructor(val value: Int) : Comparable<Year> {
    override fun compareTo(other: Year): Int = this.value.compareTo(other.value)

    operator fun inc(): Year = this.plus(1)
    operator fun plus(delta: Int): Year = Year.of(value + delta)
    operator fun minus(delta: Int): Year = Year.of(value - delta)

    operator fun minus(other: Year): Int = this.value - other.value

    companion object {
        /**
         * The minimum supported year, '-999,999,999'.
         */
        private const val MIN_VALUE = -999999999

        /**
         * The maximum supported year, '+999,999,999'.
         */
        private const val MAX_VALUE = 999999999

        private val range: ClosedRange<Int> = IntRange(MIN_VALUE, MAX_VALUE)

        private fun checkValidValue(value: Int) = try {
            requireValueIn(value, range) {
                "Invalid int value for year: $value"
            }
        } catch (e: IllegalArgumentException) {
            throw DateTimeException(e.message)
        }

        /**
         * Obtains an instance of {@code Year} from a text string such as {@code 2007}.
         * <p>
         * The string must represent a valid year.
         * Years outside the range 0000 to 9999 must be prefixed by the plus or minus symbol.
         *
         * @param text  the text to parse such as "2007", not null
         * @return the parsed year, not null
         * @throws DateTimeParseException if the text cannot be parsed
         */
        fun parse(text: String): Year {
            try {
                return Year.of(text.toInt())
            } catch (e: NumberFormatException) {
                throw DateTimeParseException()
            }
        }

        /**
         * Obtains an instance of `Year`.
         *
         * This method accepts a year value from the proleptic ISO calendar system.
         *
         * The year 2AD/CE is represented by 2.<br></br>
         * The year 1AD/CE is represented by 1.<br></br>
         * The year 1BC/BCE is represented by 0.<br></br>
         * The year 2BC/BCE is represented by -1.<br></br>
         *
         * @param isoYear  the ISO proleptic year to represent, from `MIN_VALUE` to `MAX_VALUE`
         * @return the year, not null
         * @throws DateTimeException if the field is invalid
         */
        @Suppress("DEPRECATION")
        fun of(isoYear: Int): Year = Year(checkValidValue(isoYear))

        internal fun isLeap(year: Int): Boolean {
            try {
                requireValueIn(year, 1..9999)
            } catch (e: IllegalArgumentException) {
                throw DateTimeParseException()
            }
            return (year % 4 == 0) && (year % 100 != 0 || year % 400 == 0)
        }
    }
}

/**
 * Convenient extension property to represent [Number] as [Year]. Example: `val y: Year = 1951.year`
 *
 * @return [Year] which represents [this number][Number] numeric value
 */
val Number.year: Year get() = Year.of(this.toInt())