/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.kotlin.date

enum class Month {
    JANUARY, FEBRUARY, MARCH, APRIL, MAY;

    fun getValue(): Int = this.ordinal + 1

    internal fun getOrdinal(): Int = this.ordinal

    /**
     * Gets the length of this month in days.
     *
     * This takes a flag to determine whether to return the length for a leap year or not.
     *
     * February has 28 days in a standard year and 29 days in a leap year. April, June, September
     * and November have 30 days. All other months have 31 days.
     *
     * @param leapYear  true if the length is required for a leap year
     * @return the length of this month in days, from 28 to 31
     */
    fun length(leapYear: Boolean): Int = if (this != FEBRUARY && !leapYear) {
        monthLengths[this.getOrdinal()]
    } else {
        leap_feb_len
    }

    /**
     * Gets the minimum length of this month in days.
     *
     * February has a minimum length of 28 days. April, June, September and November have 30 days.
     * All other months have 31 days.
     *
     * @return the minimum length of this month in days, from 28 to 31
     */
    fun minLength(): Int = length(false)

    /**
     * Gets the maximum length of this month in days.
     *
     * February has a maximum length of 29 days. April, June, September and November have 30 days.
     * All other months have 31 days.
     *
     * @return the maximum length of this month in days, from 29 to 31
     */
    fun maxLength(): Int = length(true)

    companion object {
        fun of(month: Int): Month {
            if (month < 1 || month > 12) {
                throw DateTimeException("Invalid value for MonthOfYear: $month")
            }
            return Month.values()[month - 1]
        }

        private const val leap_feb_len = 29

        private val monthLengths = arrayOf(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)
    }
}
