/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.kotlin.date

expect class LocalDate {
//    override fun equals(other: Any?): Boolean
//    override fun hashCode(): Int

    companion object {
        /**
         * Obtains an instance of `LocalDate` from a text string such as `2007-12-03`.
         *
         *
         * The string must represent a valid date and is parsed using
         * [org.threeten.bp.format.DateTimeFormatter.ISO_LOCAL_DATE].
         *
         * @param text  the text to parse such as "2007-12-03", not null
         * @return the parsed local date, not null
         * @throws DateTimeParseException if the text cannot be parsed
         */
        fun parse(text: CharSequence): LocalDate

        /**
         * Obtains an instance of `LocalDate` from a year, month and day.
         *
         * The day must be valid for the year and month, otherwise an exception will be thrown.
         *
         * @param year  the year to represent, from MIN_YEAR to MAX_YEAR
         * @param month  the month-of-year to represent, not null
         * @param dayOfMonth  the day-of-month to represent, from 1 to 31
         * @return the local date, not null
         * @throws DateTimeException if the value of any field is out of range
         * @throws DateTimeException if the day-of-month is invalid for the month-year
         */
        fun of(year: Int, month: Month, dayOfMonth: Int): LocalDate
    }
}
