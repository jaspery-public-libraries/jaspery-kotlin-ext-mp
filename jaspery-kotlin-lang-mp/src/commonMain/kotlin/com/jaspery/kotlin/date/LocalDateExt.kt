/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.kotlin.date

/**
 * Convenient extension property to parse [String] as [LocalDate]. Example: `val d: LocalDate = "1951-03-25".date`
 *
 * @receiver must be non-empty string which can be parsed as [LocalDate]
 * @return [LocalDate] represented by [this string][String]
 */
val String.date: LocalDate get() = LocalDate.parse(this)
