/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */
package com.jaspery.kotlin.date

class DateTimeException(message: String? = null) : Exception(message)

class DateTimeParseException : Exception()