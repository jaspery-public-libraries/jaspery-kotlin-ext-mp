/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */
package com.jaspery.kotlin.lang

/**
 * Throws an [IllegalArgumentException] with the result of calling [lazyMessage]
 * if the [value] does not match [predicate]'s condition. Otherwise returns [value].
 */
inline fun <T> require(value: T, predicate: (T) -> Boolean, lazyMessage: () -> Any): T {
    require(predicate(value), lazyMessage)
    return value
}

/**
 * Throws an [IllegalArgumentException] if the [value] does not match [predicate]'s
 * condition. Otherwise returns [value].
 */
inline fun <T> require(value: T, predicate: (T) -> Boolean): T {
    require(predicate(value))
    return value
}

/**
 * Throws an [IllegalArgumentException] with the result of calling [lazyMessage]
 * if the [value] is not in [range]. Otherwise returns [value].
 */
inline fun <T : Comparable<T>> requireValueIn(value: T, range: ClosedRange<T>, lazyMessage: () -> Any): T {
    return require(value, { it in range }) { lazyMessage() }
}

/**
 * Throws an [IllegalArgumentException] if the [value] is not in [range]. Otherwise
 * returns [value]
 */
fun <T : Comparable<T>> requireValueIn(value: T, range: ClosedRange<T>): T {
    return require(value) { it in range }
}