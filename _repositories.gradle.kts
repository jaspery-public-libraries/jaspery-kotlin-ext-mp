/*
 * Copyright © 2021. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

import com.jaspery.gradle.ext.kotlin.approvedContentJetBrainsGroup

repositories {
    mavenCentral {
        mavenContent {
            approvedContentJetBrainsGroup()
            includeModule("org.threeten", "threetenbp")
            includeModule("junit", "junit")
            // build time (jacoco)
            includeModuleByRegex("org\\.jacoco", "org\\.jacoco\\..*")
            includeModuleByRegex("org\\.ow2\\.asm", "asm.*")
            includeModule("org.ow2", "ow2")
            includeModuleByRegex("org\\.hamcrest", "hamcrest-(core|parent)")
        }
    }
}
