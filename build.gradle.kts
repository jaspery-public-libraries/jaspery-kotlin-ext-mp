/*
 * Copyright © 2020. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

import com.jaspery.gradle.ext.dependencies.junit
import com.jaspery.gradle.ext.dependencies.jvmTestImplementation
import com.jaspery.gradle.ext.jacoco.JacocoKotlinMppJvmTestReport
import com.jaspery.gradle.ext.jacoco.jacoco
import com.jaspery.gradle.ext.kotlin.*

val jitpack = System.getenv("JITPACK")?.toBoolean() ?: false

plugins {
    `kotlin-multiplatform`
    `maven-publish`
}

group = "com.jaspery.jaspery-public-libraries"
version = "1.0.0"

// dependencies
val kotlinVersion: String by project
val `310bpVersion`: String by project
val junitVersion: String by project

kotlin {
    jvm()
}

allprojects {
    apply { from("$rootDir/_repositories.gradle.kts") }
    apply { plugin("maven-publish") }
}

subprojects {
    apply { plugin(`kotlin-multiplatform`) }
    apply { plugin(jacoco) }

    group = "${rootProject.group}.${rootProject.name}"
    version = rootProject.version

    kotlin {
        jvm()
        js {
            nodejs()
            browser {
                testTask {
                    enabled = !jitpack
                    useKarma {
                        useChromeHeadless()
                    }
                }
            }
        }

        // Dependency Management
        dependencies {
            @Suppress("UnstableApiUsage")
            constraints {
                jvmTestImplementation(junit(junitVersion))
            }
        }

        // Dependencies
        commonMainDependencies {
            implementation(kotlin("stdlib-common", kotlinVersion))
        }
        commonTestDependencies {
            implementation(kotlin("test-common", kotlinVersion))
            implementation(kotlin("test-annotations-common", kotlinVersion))
        }
        jvmMainDependencies {
            implementation(kotlin("stdlib", kotlinVersion))
        }
        jvmTestDependencies {
            implementation(kotlin("test-junit", kotlinVersion))
            implementation(kotlin("reflect", kotlinVersion))
        }
        jsMainDependencies {
            implementation(kotlin("stdlib-js", kotlinVersion))
        }
        jsTestDependencies {
            implementation(kotlin("test-js", kotlinVersion))
        }

        val jacocoJvmTestReport by tasks.registering(JacocoKotlinMppJvmTestReport::class)

        val jvmTest by tasks.existing {
            finalizedBy(jacocoJvmTestReport)
        }
    }
}
